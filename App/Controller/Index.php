<?php 
namespace App\Controller;
use App\View\Template;

class Index
{

    public function index()
    {
        $template = new Template("dashboard");
        echo $template->render();
        
    }
}
