<?php namespace App\View;

/**
 * View-specific wrapper.
 * Limits the accessible scope available to templates.
 */
class Template
{

    /**
     * Template being rendered.
     */
    protected $template = null;
    protected $data = null;

    /**
     * Initialize a new view context.
     */
    public function __construct($template)
    {
        $this->template = $template;
    }

    /**
     * Safely escape/encode the provided data.
     */
    public function h($data)
    {
        return htmlspecialchars((string) $data, ENT_QUOTES, 'UTF-8');
    }

    public function teste()
    {
        echo "funcionou!";
    }

    public function setData(Array $data)
    {
        $this->data = $data;
    }

    /**
     * Render the template, returning it's content.
     * @param array $data Data made available to the view.
     * @return string The rendered template.
     */
    public function render()
    {
        if (!empty($this->data)) {
            extract($this->data);
        }

        ob_start();
        include dirname(__DIR__) . '/Config.php';
        $path = $config["app_path"];
        $assetPath = $config["app_path"] . "/assets/";
        include( dirname(__DIR__, 2) . DIRECTORY_SEPARATOR ."template" . DIRECTORY_SEPARATOR . $this->template . ".php");
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}
