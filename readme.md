# O sistema:
O desafio é desenvolver um sistema de gerenciamento de produtos. Esse sistema será composto de um cadastro de produtos e categorias sem o uso de framework.

# O que foi usado:
Usei a arquitetura MVC , juntamente com suas collections  . Usei tambem o   autoload do composer e bibliotecas de parseamento de url do mesmo.
Para rodar a ferramenta de exportação  usar 'bin/export.php'.

# Instalação:
Para rodar inicialmente , deve-se configurar o banco no arquivo :
 App/Config.php
Colocando o usuaro do banco e a senha . O nome do banco nao deve ser mudado.
Apos isso rodar o comando 'php bin/init.php'
Ele rodara o arquuivo sql data/init.sql .


